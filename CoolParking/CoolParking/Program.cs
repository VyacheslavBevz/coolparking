﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using CoolParking.BL;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;

namespace CoolParking.BL.MyProgram
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Hi");

            Console.OutputEncoding = System.Text.Encoding.UTF8;
            //создаем меню

            Menu menu = new Menu();
            //string pathToFile = Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName;
            //string fileName = Path.Combine(pathToFile, "Transactions.log");
            TimerService iWithdrawTimer = new TimerService();
            TimerService iTimeServe = new TimerService();
            ILogService iLog = new LogService(/*fileName*/);

            ParkingService _parkingService = new ParkingService(iWithdrawTimer, iTimeServe, iLog);
            menu.mainMenu(/*ParkingService.Instance*/_parkingService);
            Console.ReadKey();
        }
    }
}

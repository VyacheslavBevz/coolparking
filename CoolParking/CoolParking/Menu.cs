﻿using CoolParking.BL;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace CoolParking.BL.MyProgram
{
    class Menu
    {
        public void mainMenu(ParkingService coolPark)
        {
            //ParkingService coolPark = new ParkingService();// = ParkingService.Instance;

            bool repeat = false;
            bool exit = false;

            do
            {
                string idCar;
                decimal moneyCar;
                int refillSum;

                Console.Clear();
                Console.WriteLine("Вітаємо вас на нашій парковці CoolParking!");
                Console.WriteLine("Чого бажаєте? Натисніть, будь ласка, відповідну клавішу");
                Console.WriteLine("========================================================");
                Console.WriteLine("1.   Припаркувати авто.");
                Console.WriteLine("2.   Поповнити баланс авто.");
                Console.WriteLine("3.   Забрати авто з парковки.");
                Console.WriteLine("4.   Превірити баланс Вашого авто");
                Console.WriteLine("5.   Вивести загальньний дохід парковки.");
                Console.WriteLine("6.   Вивести кількість вільних місць на парковці.");
                Console.WriteLine("7.   Вивести історію транзакцій за останню хвилину.");
                Console.WriteLine("8.   Вивести Transactions.log");
                Console.WriteLine("9.   Показати всі авто на парковці");
                Console.WriteLine("========================================================");
                Console.WriteLine("Esc  Вихід");


                ConsoleKeyInfo keyPressed;
                keyPressed = Console.ReadKey();

                switch (keyPressed.Key)
                {
                    case ConsoleKey.D1:

                        do
                        {
                            Vehicle vehicle;// = new Vehicle();
                            bool done = false;
                            VehicleType cartype = VehicleType.AnotherType;




                            //VehicleType. cartype = Car.CarType.Undefined;
                            do
                            {


                                if (coolPark.ParkingSpace >= 1)
                                {
                                    Console.WriteLine("==========================");
                                    Console.WriteLine("Який тип Вашого авто?");
                                    Console.WriteLine("==========================");
                                    Console.WriteLine("1. PassengerCar");
                                    Console.WriteLine("2. Truck");
                                    Console.WriteLine("3. Bus");
                                    Console.WriteLine("4. Motorcycle");
                                    Console.WriteLine("==========================");
                                    Console.WriteLine("Esc. Повернутися в головне меню");
                                    keyPressed = Console.ReadKey();
                                    switch (keyPressed.Key)

                                    {
                                        case ConsoleKey.D1:
                                            cartype = VehicleType.PassengerCar;
                                            done = true;
                                            break;
                                        case ConsoleKey.D2:
                                            cartype = VehicleType.Truck;
                                            done = true;
                                            break;
                                        case ConsoleKey.D3:
                                            cartype = VehicleType.Bus;
                                            done = true;
                                            break;
                                        case ConsoleKey.D4:
                                            cartype = VehicleType.Motorcycle;
                                            done = true;
                                            break;
                                        case ConsoleKey.Escape:
                                            done = true;
                                            break;
                                        default:
                                            Console.Clear();
                                            Console.WriteLine("На жаль, такого типу авто не існує");
                                            Console.WriteLine("Натисніть будь-яку клавішу");
                                            Console.ReadKey();
                                            continue;

                                    }

                                }
                                else
                                {
                                    done = true;
                                    Console.Clear();
                                    Console.WriteLine("На разі всі місця зайняті! Зверніться пізніше!");
                                    Console.WriteLine("Для повернення в основне меню натисніть будь-яку клавішу");
                                    Console.ReadKey();
                                }
                            }
                            while (!done);

                            if (cartype != VehicleType.AnotherType)
                            {
                                Console.Clear();
                                Console.WriteLine("==========================");
                                Console.WriteLine("Введите номера вашего автомобиля в формате ХХ-YYYY-XX (где X - любая буква английского алфавита в верхнем регистре, а Y - любая цифра, например DV-2345-KJ).");
                                //idCar = "AA-0000-AA";
                                idCar = Console.ReadLine();
                                Console.WriteLine("==========================");
                                Console.WriteLine("Сумма");
                                moneyCar = Decimal.Parse(Console.ReadLine());
                                vehicle = new Vehicle(idCar, cartype, moneyCar);
                                coolPark.AddVehicle(vehicle);
                            }
                            else
                            {
                                Console.Clear();
                                this.mainMenu(coolPark);
                            }

                            Console.Clear();
                            Console.WriteLine("==============================================================");
                            Console.WriteLine("Ваше авто успішно припарковано!");
                            Console.WriteLine("==============================================================");
                            Console.WriteLine("Припаркувати ще одне авто?  ---  Натисніть Enter.");
                            Console.WriteLine("Повернутися в головне меню? ---  Натисніть будь-яку клавішу");
                            Console.WriteLine("==============================================================");
                            keyPressed = Console.ReadKey();
                            if (keyPressed.Key == ConsoleKey.Enter)
                            {
                                repeat = true;
                                Console.Clear();
                            }
                            else
                            {
                                repeat = false;
                            }
                        }
                        while (repeat == true);
                        Console.Clear();

                        break;
                    case ConsoleKey.D2:

                        do
                        {

                            Console.Clear();
                            Console.WriteLine("Какой номер вашего автомобиля в формате ХХ-YYYY-XX?");
                            idCar = "AA-0000-AA";
                            //idCar = Console.ReadLine();
                            Console.WriteLine("Введіть суму поповнення:");
                            refillSum = Int32.Parse(Console.ReadLine());

                            coolPark.TopUpVehicle(idCar, refillSum);



                            Console.WriteLine("==============================================================");
                            Console.WriteLine("Поповнити баланс іншого авто ---   Натисніть Enter.");
                            Console.WriteLine("Повернутися в головне меню    ---   Натисніть будь-яку клавішу");
                            Console.WriteLine("==============================================================");

                            keyPressed = Console.ReadKey();
                            if (keyPressed.Key == ConsoleKey.Enter)
                            {
                                repeat = true;
                                Console.Clear();
                            }
                            else
                            {
                                repeat = false;
                            }
                        }
                        while (repeat == true);
                        Console.Clear();

                        break;

                    case ConsoleKey.D3:

                        do
                        {
                            Console.Clear();
                            Console.WriteLine("Какой номер вашего автомобиля в формате ХХ-YYYY-XX?");
                            idCar = "AA-0000-AA";
                            //idCar = Console.ReadLine();
                            coolPark.RemoveVehicle(idCar);
                            Console.WriteLine("==============================================================");
                            Console.WriteLine("Ви забрали своє авто з парковки!");
                            Console.WriteLine("==============================================================");
                            Console.WriteLine("Забрати інше авто з парковки ---  Натисніть Enter.");
                            Console.WriteLine("Повернутися в головне меню   ---  Натисніть будь-яку клавішу");
                            Console.WriteLine("==============================================================");

                            keyPressed = Console.ReadKey();
                            if (keyPressed.Key == ConsoleKey.Enter)
                            {
                                repeat = true;
                                Console.Clear();
                            }
                            else
                            {
                                repeat = false;
                            }
                        }
                        while (repeat == true);
                        Console.Clear();

                        break;

                    case ConsoleKey.D4:

                        do
                        {
                            Console.Clear();
                            Console.WriteLine("Какой номер вашего автомобиля в формате ХХ-YYYY-XX?");
                            //idCar = Console.ReadLine();
                            //TODO: Закоментировать тестовые значения.
                            idCar = "AA-0000-AA";
                            coolPark.GetCarBalance(idCar);

                            Console.WriteLine("==============================================================");
                            Console.WriteLine("Перевірити баланс іншого авто? ---   Натисніть Enter.");
                            Console.WriteLine("Повернутися в головне меню?    ---   Натисніть будь-яку клавішу");
                            Console.WriteLine("==============================================================");

                            keyPressed = Console.ReadKey();
                            if (keyPressed.Key == ConsoleKey.Enter)
                            {
                                repeat = true;
                                Console.Clear();
                            }
                            else
                            {
                                repeat = false;
                            }
                        }
                        while (repeat == true);
                        Console.Clear();

                        break;

                    case ConsoleKey.D5:

                        do
                        {
                            coolPark.GetBalance();

                            Console.WriteLine("==============================================================");
                            Console.WriteLine("Бажаєте спробувати ще раз?");
                            Console.WriteLine("Перевірити баланс ще раз?   --- Натисніть Enter.");
                            Console.WriteLine("Повернутися в головне меню? --- Натисніть будь-яку клавішу");
                            Console.WriteLine("==============================================================");

                            keyPressed = Console.ReadKey();
                            if (keyPressed.Key == ConsoleKey.Enter)
                            {
                                repeat = true;
                                Console.Clear();
                            }
                            else
                            {
                                repeat = false;
                            }
                        }
                        while (repeat == true);
                        Console.Clear();

                        break;
                    case ConsoleKey.D6:

                        do
                        {
                            coolPark.GetFreePlaces();
                            Console.WriteLine("==============================================================");
                            Console.WriteLine("Бажаєте спробувати ще раз?");
                            Console.WriteLine("Перевірити кількість місць ще раз? - Натисніть Enter.");
                            Console.WriteLine("Повернутися в головне меню? - Натисніть будь-яку клавішу");
                            Console.WriteLine("==============================================================");

                            keyPressed = Console.ReadKey();
                            if (keyPressed.Key == ConsoleKey.Enter)
                            {
                                repeat = true;
                                Console.Clear();
                            }
                            else
                            {
                                repeat = false;
                            }
                        }
                        while (repeat == true);
                        Console.Clear();

                        break;
                    case ConsoleKey.D7:

                        do
                        {
                            coolPark.TransactionLastMinute();
                            Console.WriteLine("==============================================================");
                            Console.WriteLine("Перевірити баланс іншого авто? - Натисніть Enter.");
                            Console.WriteLine("Повернутися в головне меню? - Натисніть будь-яку клавішу");
                            Console.WriteLine("==============================================================");

                            keyPressed = Console.ReadKey();
                            if (keyPressed.Key == ConsoleKey.Enter)
                            {
                                repeat = true;
                                Console.Clear();
                            }
                            else
                            {
                                repeat = false;
                            }
                        }
                        while (repeat == true);
                        Console.Clear();

                        break;


                    case ConsoleKey.D8:

                        do
                        {
                            coolPark.ReadFromLog();
                            Console.WriteLine("==============================================================");
                            Console.WriteLine("Бажаєте спробувати ще раз?");
                            Console.WriteLine("Вивести лог ще раз? - Натисніть Enter.");
                            Console.WriteLine("Повернутися в головне меню? - Натисніть будь-яку клавішу");
                            Console.WriteLine("==============================================================");
                            keyPressed = Console.ReadKey();
                            if (keyPressed.Key == ConsoleKey.Enter)
                            {
                                repeat = true;
                                Console.Clear();
                            }
                            else
                            {
                                repeat = false;
                            }
                        }
                        while (repeat == true);
                        Console.Clear();

                        break;

                    case ConsoleKey.D9:

                        do
                        {
                            Console.Clear();
                            if (coolPark.ParkingSpace  > 0)
                            {

                                Console.WriteLine("На парковці {0} авто", coolPark.ParkingSpace);

                                foreach (var vehicle in coolPark.GetVehicles())

                                {
                                    Console.WriteLine("{0} --- {1} --- {2}", vehicle.Id, vehicle.VehicleOfType, vehicle.Balance);
                                }
                            }
                            else Console.WriteLine("На парковці наразі жодного авто)");

                            Console.WriteLine("==============================================================");
                            Console.WriteLine("Бажаєте спробувати ще раз?");
                            Console.WriteLine("Вивести лог ще раз? - Натисніть Enter.");
                            Console.WriteLine("Повернутися в головне меню? - Натисніть будь-яку клавішу");
                            Console.WriteLine("==============================================================");
                            keyPressed = Console.ReadKey();
                            if (keyPressed.Key == ConsoleKey.Enter)
                            {
                                repeat = true;
                                Console.Clear();
                            }
                            else
                            {
                                repeat = false;
                            }
                        }
                        while (repeat == true);
                        Console.Clear();

                        break;



                    case ConsoleKey.Escape:
                        Console.WriteLine("Have a nice day)");
                        exit = true;
                        return;

                    default:
                        Console.WriteLine("Oooops!! Зробіть Ваш вибір");
                        continue;
                }
            }

            while (!exit);
        }

    }
}


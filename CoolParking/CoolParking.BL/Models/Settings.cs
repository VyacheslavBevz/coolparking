﻿// TODO: +implement class Settings.
//       +Implementation details are up to you, they just have to meet the requirements of the home task.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    static class Settings
    {

        /// <summary>
        /// <param name="BalanseParking">Начальный баланс Паркинга</param>
        /// <param name="ParkingSpace">Вместимость Паркинга</param>
        /// <param name="BillingPeriod">Период списания оплаты, N-секунд</param>
        /// <param name="RecordingPeriodInLog">Период записи в лог, N-секунд</param>
        /// <param name="Rate">Тарифы в зависимости от Тр. средства: Легковое, Грузовое , Автобус , Мотоцикл ;</param>
        /// <param name="Fine">Коэффициент штрафа</param>
        /// </summary>
        public static double BalanseParking { get; } = 0;
        public static int ParkingSpace { get; } = 10;
        public static int BillingPeriod { get; } = 5;
        public static int RecordingPeriodInLog { get; } = 60;

        public static Dictionary<VehicleType, decimal> Rate { get; set; } = new Dictionary<VehicleType, decimal>
    {
    { VehicleType.PassengerCar , 2},
    { VehicleType.Truck , 5},
    { VehicleType.Bus , (decimal)3.5},
    { VehicleType.Motorcycle , 1}
    };
        public static decimal Fine { get; } = (decimal)2.5;
    }
}
﻿// TODO: +implement struct TransactionInfo.
//       +Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;
using System.Threading;

namespace CoolParking.BL.Models
{

    public class TransactionInfo
    {
        public int IdTransaction { get; set; }
        public DateTime TimeTransaction { get; set; }
        public string IdCar { get; set; }
        public decimal Sum { get; set; }
        /// <summary>
        /// TODO: +need add IdTransactionGlobal or something like that
        /// </summary>
        public static int IdTransactionGlobal;

        public TransactionInfo(DateTime timeTransaction, string idCar, decimal sum)
        {
            this.IdTransaction = Interlocked.Increment(ref IdTransactionGlobal);
            this.TimeTransaction = timeTransaction;
            this.IdCar = idCar;
            this.Sum = sum;
        }
    }
}
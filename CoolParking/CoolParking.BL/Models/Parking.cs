﻿// TODO: +implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
namespace CoolParking.BL.Models
{
    public class Parking
    {
        //private static readonly Lazy<Parking> lazy = new Lazy<Parking>(() => new Parking());

        //public Parking Instance
        //{
        //    get
        //    {
        //        return lazy.Value;
        //    }
        //}
        public decimal BalanseParking { get; set; }
        public List<Vehicle> VehicleArray { get; set; }


        /* //LogService _logService = new LogService();
         /// <summary>
         /// <param name="BalanseParking">Баланс Паркинга</param>
         /// <param name="VehicleArray">Коллекция Тр. средств</param>
         /// <param name="ParkingSpace">Вместимость Паркинга</param>
         /// <param name="BillingPeriod">Период списания оплаты, N-секунд</param>
         /// <param name="RecordingPeriodInLog">Период записи в лог, N-секунд</param>
         /// <param name="Rate">Тарифы в зависимости от Тр. средства: Легковое, Грузовое , Автобус , Мотоцикл ;</param>
         /// <param name="Fine">Коэффициент штрафа</param>
         /// </summary>
         /// 
         public decimal BalanseParking { get; set; }
         public List<Vehicle> VehicleArray { get; set; }
         //public List<TransactionInfo> Transaction { get; set; }
         public List<TransactionInfo> Transaction { get; set; }
         public int ParkingSpace { get; set; }

         public int BillingPeriod { get; set; }
         public int RecordingPeriodInLog { get; set; }

         public Dictionary<Vehicle.VehicleType, double> Rate { get; }
         public double Fine { get; set; }

         public Parking()
         {

             this.VehicleArray = new List<Vehicle>();
             this.Transaction = new List<TransactionInfo>();
             //this.Transaction = new TransactionInfo[]();
             this.ParkingSpace = Settings.ParkingSpace;
             this.BillingPeriod = Settings.BillingPeriod;
             this.RecordingPeriodInLog = Settings.RecordingPeriodInLog;
             this.Fine = Settings.Fine;

             // TODO: Добавить само списывание средств BillingPeriod и RecordingPeriodInLog, может в таске, или что-то в этом духе
             Task.Run(() => ChargeOffAsync(1000 * BillingPeriod));
             Task.Run(() => ChargeOffLog(1000 * RecordingPeriodInLog));

         }
         private async Task ChargeOffAsync(int delay)
         {
             while (true)
             {
                 if (VehicleArray.Count >= 1)
                 {
                     foreach (var vehicle in VehicleArray)
                     {
                         double rate = 0;

                         //PassengerCar, Truck, Bus, Motorcycle
                         switch (vehicle.VehicleOfType)
                         {
                             case Vehicle.VehicleType.PassengerCar:
                                 rate = Rate[Vehicle.VehicleType.PassengerCar];
                                 break;
                             case Vehicle.VehicleType.Truck:
                                 rate = Rate[Vehicle.VehicleType.Truck];
                                 break;
                             case Vehicle.VehicleType.Bus:
                                 rate = Rate[Vehicle.VehicleType.Bus];
                                 break;
                             case Vehicle.VehicleType.Motorcycle:
                                 rate = Rate[Vehicle.VehicleType.Motorcycle];
                                 break;
                             default: break;
                         }
                         if (vehicle.Balance < (decimal)rate)
                         {
                             rate += rate * Fine;
                         }
                         vehicle.Balance -= (decimal)rate;
                         BalanseParking += (decimal)rate;

                         TransactionInfo transactionInfo = new TransactionInfo(DateTime.Now, vehicle.Id, rate);
                         Transaction.Add(transactionInfo);
                     }
                 }
                 await Task.Delay(delay);
             }


         }
         private async Task ChargeOffLog(int delay)
         {
             while (true)
             {
                 double sum = 0;
                 foreach (var trans in Transaction)
                 {
                     if (trans.TimeTransaction > (DateTime.Now - TimeSpan.FromSeconds(60)))
                     {
                         sum += trans.Sum;
                     }
                 }
                 string log = string.Format("Час: {0}, Сума транзакцій за {1} сек: {2} у.е.", DateTime.Now, Settings.RecordingPeriodInLog, sum);

                 //_logService.Write(log);



                 string pathToFile = Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName;
                 string fileName = Path.Combine(pathToFile, "Transactions.log");

                 using (StreamWriter file = new StreamWriter(fileName))
                 {
                     file.WriteLine(log);
                 }

                 await Task.Delay(delay);



             }
         }*/


    }
}
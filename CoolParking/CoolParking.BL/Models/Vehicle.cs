﻿// TODO: implement class Vehicle.
//       +Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       +The format of the identifier is explained in the description of the home task.
//       +Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.
using System;
using System.Data.Common;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; private set; }
        public VehicleType VehicleOfType { get; }
        public decimal Balance { get; set; }
        //public enum VehicleType { AnotherType, PassengerCar, Bus, Truck, Motorcycle }
        //public Vehicle.VehicleType VehicleOfType { get; set; }
        public Vehicle()
        {
        }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            if (Regex.IsMatch(id, patternId))
            {
                this.Id = id;
                this.VehicleOfType = vehicleType;
                this.Balance = balance;
            }
            else{
                throw new System.InvalidOperationException();
            }


        }
        string patternId = @"[A-Z]{2}-[0-9]{4}-[A-Z]{2}";
    }
    //var vehicle1 = new Vehicle("AA-0001-AA", VehicleType.Bus, 100);
}
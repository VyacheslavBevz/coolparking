﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using System.Timers;
using CoolParking.BL.Interfaces;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        public Timer timer = new Timer();
        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this, null);
        }

        public void Dispose()
        {
            timer.Dispose();
        }

        public void Start()
        {
            timer.Interval = 5;
            timer.Start();
        }

        public void Stop()
        {
            timer.Stop();
        }

    }
}


////private Timer timer = new Timer();

//public double Interval { get; set; }

//public event ElapsedEventHandler Elapsed;
////{
////    add { this.timer.Elapsed += value; }
////    remove { this.timer.Elapsed -= value; }
////}

//public void Dispose()
//{
//    //timer.Dispose();
//}

//public void Start()
//{
//    //timer.Interval = 5;
//    //timer.Start();
//}

//public void Stop()
//{
//    //timer.Stop();
//}
//public void FireElapsedEvent()
//{
//    Elapsed?.Invoke(this, null);
//}


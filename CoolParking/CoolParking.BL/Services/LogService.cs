﻿// TODO: +implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.
using CoolParking.BL.Interfaces;
using System;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Threading;
namespace CoolParking.BL.Services
{


    public class LogService : ILogService
    {
        private readonly string _logFilePath = $@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log";
        private readonly ILogService _logService;

        //   public string logFilePath;
        public LogService(string logFilePath)
        {
            this.LogPath = logFilePath;
        }
        public LogService()
        {
            _logService = new LogService(_logFilePath);
        }

        public string LogPath { get; }

        //public string LogPath => Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName;

        public string Read()
        {
            //string pathToFile = Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName;

            string totalRead;
            try
            {
                using (StreamReader sr = new StreamReader(LogPath))
                {
                    Console.WriteLine(sr.ReadToEnd());
                    totalRead = sr.ReadToEnd();

                }

                // асинхронное чтение
                /* using (StreamReader sr = new StreamReader(fileName))
                 {
                     Console.WriteLine(await sr.ReadToEndAsync());
                 }*/
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                throw new InvalidOperationException();
            }
            /*
            string[] lines = new string[] { };

            bool readCompleted = false;

            while(!readCompleted){
            try{
                    lines = File.ReadAllLines(fileName);
                    readCompleted = true;
            }
            catch(Exception){
                    Thread.Sleep(5000);
                    readCompleted = false;
                }
            }

            Console.WriteLine("Log за останні {0} секунд", Settings.RecordingPeriodInLog);
            foreach(string line in lines){
                Console.WriteLine(line);
            }
            */
            return totalRead;
        }

        public void Write(string logInfo)
        {
            //string pathToFile = Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName;
            //string fileName = Path.Combine(/*pathToFile*/logInfo, "Transactions.log");
            string fileName = /*pathToFile*/LogPath;

            using (StreamWriter file = new StreamWriter(fileName))
            {
                file.WriteLine(logInfo);
            }
        }
    }
}
﻿// TODO: +implement the ParkingService class from the IParkingService interface.
// TODO: Parking must be Singleton somethimg like lazy
// TODO: Parking and ParkingService разделить.
//       +For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       +For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;


//CS1503 C# Аргумент 1: не удается преобразовать из "CoolParking.BL.Tests.FakeTimerService [C:\Users\PARACORD\Documents\GitHub\coolparking\CoolParking\CoolParking.BL.Tests\FakeTimerService.cs(6)]" в "CoolParking.BL.Tests.FakeTimerService [CoolParking.BL, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null]".
//пытаюсь костыль из-за того что 3 часа не могу исправить/найти инфу
//using CoolParking.BL.Services;
//using CoolParking.BL.Tests;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        //LogService _logService = new LogService();
        /// <summary>
        /// <param name="BalanseParking">Баланс Паркинга</param>
        /// <param name="VehicleArray">Коллекция Тр. средств</param>
        /// <param name="ParkingSpace">Вместимость Паркинга</param>
        /// <param name="BillingPeriod">Период списания оплаты, N-секунд</param>
        /// <param name="RecordingPeriodInLog">Период записи в лог, N-секунд</param>
        /// <param name="Rate">Тарифы в зависимости от Тр. средства: Легковое, Грузовое , Автобус , Мотоцикл ;</param>
        /// <param name="Fine">Коэффициент штрафа</param>
        /// </summary>
        /// 
        //public decimal BalanseParking { get; set; }
        //public List<Vehicle> VehicleArray { get; set; }
        public List<TransactionInfo> Transaction { get; set; }
        public int ParkingSpace { get; set; }

        public int BillingPeriod { get; set; }
        public int RecordingPeriodInLog { get; set; }

        public Dictionary<VehicleType, decimal> Rate { get; }
        public decimal Fine { get; set; }

        public static Parking parking;

        public static ILogService ilogService;
        public static readonly ITimerService iWithdrawTimer;
        public static readonly ITimerService iTimeServe;

        public LogService logService;
        public TimerService withdrawTimer;
        public TimerService timeServe;

        //private static readonly Lazy<ParkingService> lazy = new Lazy<ParkingService>(() => new ParkingService(iWithdrawTimer, iTimeServe, ilogService));

        public ParkingService(ITimerService iWithdrawTimer, ITimerService iTimeServe, ILogService ilogService)
        {
            if (parking == null)
            {
                parking = new Parking();
            }
            

            this.withdrawTimer = (TimerService)iWithdrawTimer;
            this.timeServe = (TimerService)iTimeServe;
            this.logService = (LogService)ilogService;

            parking.VehicleArray = new List<Vehicle>();
            this.Transaction = new List<TransactionInfo>();
            this.ParkingSpace = Settings.ParkingSpace;
            this.BillingPeriod = Settings.BillingPeriod;
            this.RecordingPeriodInLog = Settings.RecordingPeriodInLog;
            this.Rate = Settings.Rate;
            this.Fine = Settings.Fine;

            Task.Run(() => ChargeOffAsync(1000 * BillingPeriod));
            Task.Run(() => ChargeOffLog(1000 * RecordingPeriodInLog));
        }


        //public ParkingService Instance
        //{
        //    get
        //    {
        //        return lazy.Value;
        //    }
        //}


        /* public ParkingService()
         {

             this.VehicleArray = new List<Vehicle>();
             this.Transaction = new List<TransactionInfo>();
             //this.Transaction = new TransactionInfo[]();
             this.ParkingSpace = Settings.ParkingSpace;
             this.BillingPeriod = Settings.BillingPeriod;
             this.RecordingPeriodInLog = Settings.RecordingPeriodInLog;
             this.Rate = Settings.Rate;
             this.Fine = Settings.Fine;

             // TODO: Добавить само списывание средств BillingPeriod и RecordingPeriodInLog, может в таске, или что-то в этом духе
             Task.Run(() => ChargeOffAsync(1000 * BillingPeriod));
             Task.Run(() => ChargeOffLog(1000 * RecordingPeriodInLog));
         }*/

        public decimal GetBalance()
        {
            Console.WriteLine("На цю хвилину дохід парковки становить, {0} грн.", parking.BalanseParking);
            return parking.BalanseParking;
        }

        public void GetCarBalance(string carId)
        {

            try
            {
                Vehicle vehicle1 = parking.VehicleArray.Find(x => x.Id == carId);
                Console.Clear();
                Console.WriteLine("Баланс Вашого авто {0} грн. ", vehicle1.Balance.ToString());
                Console.ReadLine();

            }
            catch

            {
                Console.WriteLine("На жаль, таке авто у нас не припарковане");
                Console.WriteLine("Натисніть будь-яку клавішу...");
                Console.ReadLine();
            }

        }
        public int GetCapacity()
        {
            return Settings.ParkingSpace;

        }

        public int GetFreePlaces()
        {
            Console.WriteLine("Зараз доступно {0} вільних місць з {1} існуючих на парковці", this.ParkingSpace, Settings.ParkingSpace);
            return this.ParkingSpace;
        }



        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return parking.VehicleArray.AsReadOnly();

        }

        public void AddVehicle(Vehicle vehicle)
        {
            Vehicle vehicle1 = parking.VehicleArray.Find(x => x.Id == vehicle.Id);
            if (this.ParkingSpace > 0)
            {
                if (vehicle1 == null)
                {
                    //vehicle.VehicleOfType veh = new vehicle.VehicleOfType();
                    //Vehicle veh = new Vehicle(vehicle.Id, vehicle.VehicleOfType, vehicle.Balance);
                    parking.VehicleArray.Add(vehicle);
                    this.ParkingSpace--;
                }
                else{
                  throw  new System.InvalidOperationException();
                }
            }
            else
            {
                throw new System.InvalidOperationException();
            }

        }

        public void RemoveVehicle(string vehicleId)
        {

            Vehicle removeVehicle = parking.VehicleArray.Find(x => x.Id == vehicleId);
            //TODO: If Vehicle out
            try
            {
                //TODO: +If balance <0
                if (removeVehicle.Balance < 0)
                {
                    Console.WriteLine("Не хватка коштів на балансі! \nПоповніть баланс і після того зможете забрати авто.");
                    throw new System.InvalidOperationException();
                }
                else
                {

                    parking.VehicleArray.Remove(removeVehicle);
                    this.ParkingSpace++;
                }
            }
            catch
            {
                Console.WriteLine("В списку авто з таким номером не знайдено!");
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            try
            {
                Vehicle vehicle1 = parking.VehicleArray.Find(x => x.Id == vehicleId);
                vehicle1.Balance += sum;
                Console.Clear();

                Console.WriteLine("Внесено {0} грн. ", sum.ToString());
                Console.WriteLine("Баланс Вашого авто {0} грн. ", vehicle1.Balance.ToString());

            }
            catch
            {
                Console.WriteLine("На жаль, таке авто у нас не припарковане");
                Console.WriteLine("Натисніть будь-яку клавішу...");
                Console.ReadLine();
            }


        }

        public void TransactionLastMinute()
        {
            Console.Clear();
            foreach (var tr in Transaction)
            {
                if (tr.TimeTransaction > (DateTime.Now - TimeSpan.FromMinutes(1)))
                    Console.WriteLine(" {0} з авто № {1} {2} списано {3} грн", tr.IdTransaction, tr.IdCar, tr.TimeTransaction, tr.Sum);
            }
        }
        public TransactionInfo[] GetLastParkingTransactions()
        {
            TransactionInfo tr = this.Transaction.Last();
            TransactionInfo[] tr1 = { tr };
            return tr1;

        }
        public string ReadFromLog()
        {
            return logService.Read();
            /*
            string[] lines = new string[] { };
            bool readComplate = false;
            while(!readComplate){
            try{
                    lines = File.ReadAllLines(this.logService.LogPath);
                    readComplate = true;
            }
            catch(Exception){
                    Thread.Sleep(5000);
            }
            }

            Console.WriteLine("Log всіх транзакцій по хвилинам");
            foreach(string line in lines){
                Console.WriteLine("\t" + line);
            }
            return totalRead;
            */
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }
        private async Task ChargeOffAsync(int delay)
        {
            while (true)
            {
                if (parking.VehicleArray.Count >= 1)
                {
                    foreach (var vehicle in parking.VehicleArray)
                    {
                        decimal rate = 0;

                        //PassengerCar, Truck, Bus, Motorcycle
                        switch (vehicle.VehicleOfType)
                        {
                            case VehicleType.PassengerCar:
                                rate = Rate[VehicleType.PassengerCar];
                                break;
                            case VehicleType.Truck:
                                rate = Rate[VehicleType.Truck];
                                break;
                            case VehicleType.Bus:
                                rate = Rate[VehicleType.Bus];
                                break;
                            case VehicleType.Motorcycle:
                                rate = Rate[VehicleType.Motorcycle];
                                break;
                            default: break;
                        }
                        if (vehicle.Balance < (decimal)rate)
                        {
                            rate += rate * Fine;
                        }
                        vehicle.Balance -= (decimal)rate;
                        parking.BalanseParking += (decimal)rate;

                        TransactionInfo transactionInfo = new TransactionInfo(DateTime.Now, vehicle.Id, rate);
                        Transaction.Add(transactionInfo);
                    }
                }
                await Task.Delay(delay);
            }


        }
        private async Task ChargeOffLog(int delay)
        {
            while (true)
            {
                decimal sum = 0;
                foreach (var trans in Transaction)
                {
                    if (trans.TimeTransaction > (DateTime.Now - TimeSpan.FromSeconds(60)))
                    {
                        sum += trans.Sum;
                    }
                }
                string log = string.Format("Час: {0}, Сума транзакцій за {1} сек: {2} у.е.", DateTime.Now, Settings.RecordingPeriodInLog, sum);

                logService.Write(log);



                /*string pathToFile = Directory.GetParent(System.Reflection.Assembly.GetExecutingAssembly().Location).FullName;
                string fileName = Path.Combine(pathToFile, "Transactions.log");

                using (StreamWriter file = new StreamWriter(fileName))
                {
                    file.WriteLine(log);
                }
                */
                await Task.Delay(delay);



            }
        }
    }
}